package com.example.firebaseapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    private EditText emailInput, passwordInput;
    private Button loginButton, signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        auth = FirebaseAuth.getInstance();

        emailInput = findViewById(R.id.email);
        passwordInput = findViewById(R.id.password);

        loginButton = findViewById(R.id.loginButton);
        signUpButton = findViewById(R.id.signUpButton);
    }

    public void onClickLoginButton(View view) {
        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();

        if (oneIsEmpty(email, password)) {
            Toast.makeText(this, "Please, fill all the fields", Toast.LENGTH_LONG).show();
        } else {
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                goToHelloWorldActivity("Successfully logged in!");
                            } else {
                                operationFailed("Login failed");
                            }
                        }
                    });
        }

    }

    public void onClickSignUpButton(View view) {
        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();

        if (oneIsEmpty(email, password)) {
            Toast.makeText(this, "Please, fill all the fields", Toast.LENGTH_LONG).show();
        } else {
            auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                goToHelloWorldActivity("Successfully registered!");
                            } else {
                                operationFailed("Register failed");
                            }
                        }
                    });
        }
    }

    private void goToHelloWorldActivity(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, Welcome.class);
        startActivity(intent);
        finish();
    }

    private void operationFailed(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private boolean oneIsEmpty(String email, String password) {
        return TextUtils.isEmpty(email) || TextUtils.isEmpty(password);
    }
}
