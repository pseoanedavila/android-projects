package com.example.coronaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void displayRepos(View view) {
        EditText username = (EditText) findViewById(R.id.userNameInput);
        Intent intent = new Intent(MainActivity.this, reposNames.class);
        intent.putExtra("username", username.getText().toString());
        startActivity(intent);
    }

}
