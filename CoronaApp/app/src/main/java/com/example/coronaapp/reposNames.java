package com.example.coronaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import responseObjects.UserRepo;

public class reposNames extends AppCompatActivity {
    private RepoService service;
    private String username;
    @NonNull
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repos_names);
        username = getIntent().getStringExtra("username");
        service = new RetrofitHelper().getRepoService();
        requestReposNames();
    }

    private void requestReposNames() {
        mCompositeDisposable.add(service.listRepos(username)
                .subscribeOn(Schedulers.io()) // "work" on io thread)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<UserRepo>>() {
                    @Override
                    public void accept(
                            @io.reactivex.annotations.NonNull final List<UserRepo> userRepos) {
                                displayRepos(userRepos);
                    }

                })
        );

    }

    private void displayRepos(List<UserRepo> repos) {
        Picasso.with(this).load(repos.get(0).owner.avatar_url).into((ImageView)findViewById(R.id.userAvatar));
        TextView usernameField = findViewById(R.id.userName);
        usernameField.setText(username);
        ListView reposList = (ListView) findViewById(R.id.reposNamesList);
        ArrayList<HashMap<String, String>> list = new ArrayList();
        SimpleAdapterWithViewHolder adapter = new SimpleAdapterWithViewHolder(this, list,
                R.layout.repository_info, new String[] {"repoName"}, new int[]{R.id.repoName});
        reposList.setAdapter(adapter);
        for (UserRepo repo : repos) {
            HashMap<String, String> map = new HashMap<>();
            map.put("repoName", repo.name);
            list.add(map);
        }

    }
}
