package com.example.coronaapp;

import java.util.List;

import io.reactivex.Single;
import responseObjects.UserRepo;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RepoService {
    @GET("users/{user}/repos")
    Single<List<UserRepo>> listRepos(@Path("user") String user);
}
