package com.example.coronaapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleAdapterWithViewHolder extends SimpleAdapter {
    private Context context;
    public SimpleAdapterWithViewHolder(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder repo;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.repository_info, null);
            repo = new ViewHolder();
            repo.repoName = convertView.findViewById(R.id.repoName);
            convertView.setTag(repo);
        } else {
            repo = (ViewHolder) convertView.getTag();
        }

        repo.repoName.setText(((HashMap<String, String>) getItem(position)).get("repoName"));
        return convertView;
    }

    private  static class ViewHolder {
        TextView repoName;
    }
}
