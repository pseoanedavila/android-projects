package com.example.periodicos;

import android.os.AsyncTask;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class NewspapersNamesLoader extends AsyncTask<URL, Integer, Long> {
    private ArrayList<String> list;
    private ArrayAdapter adapter;
    NewspapersNamesLoader(ArrayList<String> list, ArrayAdapter adapter) {
        this.list = list;
        this.adapter = adapter;
    }
    @Override
    protected Long doInBackground(URL... urls) {
        Document document;
        try {
            for (URL url: urls) {
                document = Jsoup.connect(url.toString()).get();
                list.add(document.title());
                adapter.notifyDataSetChanged();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
