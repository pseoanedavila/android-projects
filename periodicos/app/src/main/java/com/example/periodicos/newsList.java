package com.example.periodicos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
;
import java.util.ArrayList;
import java.util.HashMap;

public class newsList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        ListView headlinesList = (ListView) findViewById(R.id.newsList);
        ArrayList<HashMap<String, String>> list = new ArrayList();

        //Usar patrón view holder

        SimpleAdapterWithViewHolder adapter = new SimpleAdapterWithViewHolder(this, list,
                R.layout.fila_noticia, new String[] {"headline"}, new int[]{R.id.headline});

                //inal ArrayAdapter adapter = new ArrayAdapter(this, R.layout.fila_noticia, list); //Expandable?
        headlinesList.setAdapter(adapter);
        NewsLoader task = new NewsLoader(this, list, adapter);
        task.execute();
    }
}
