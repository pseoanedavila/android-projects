package com.example.periodicos;

import android.os.AsyncTask;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class NewsLoader extends AsyncTask<URL, Integer, Long> {
    private AppCompatActivity activity;
    private ArrayList list;
    private ArrayAdapter adapter;
    NewsLoader(AppCompatActivity activity, ArrayList<HashMap<String, String>> list, ArrayAdapter adapter) {
        this.activity = activity;
        this.list = list;
        this.adapter = adapter;
    }
    @Override
    protected Long doInBackground(URL... urls) {
        try {
            String url = activity.getIntent().getStringExtra("url");
            String className = activity.getIntent().getStringExtra("className");
            Document document = Jsoup.connect(url).get();
            Elements elements = document.select(className);
            for (Element headline : elements) {
                list.add(headline.text());
                if (list.size()  >= 30) {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(Long result) {
        adapter.notifyDataSetChanged();
    }
}
