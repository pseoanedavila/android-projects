package com.example.periodicos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class SimpleAdapterWithViewHolder extends SimpleAdapter {
    private Context context;
    public SimpleAdapterWithViewHolder(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        this.context = context;
    }

    @Override
    /*
    parent: la lista
    convertView: la fila
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder noticia;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.fila_noticia, null);
            noticia = new ViewHolder();
            noticia.titulo = convertView.findViewById(R.id.headline);

        } else {
            noticia = (ViewHolder) convertView.getTag();
        }


        noticia.titulo.setText( ((HashMap<String, String>) getItem(position)).get("titulo") );

    }

    private static class ViewHolder {
        TextView titulo;
    }
}
