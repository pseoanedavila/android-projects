package com.example.periodicos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView newsPapersNames = (ListView) findViewById(R.id.listaPeriodicos);
        String[] values = new String[] {"El País", "Público", "El Mundo"};
        final String[] urls = new String[] {"https://www.elpais.com/", "https://publico.es", "https://elmundo.es/"};
        final String[] classNames = new String[] {"h2.headline", "a.page-link", "h2.ue-c-cover-content__headline"};
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < values.length; i++) {
            list.add(values[i]);
        }
        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        newsPapersNames.setAdapter(adapter);
        newsPapersNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, newsList.class);
                intent.putExtra("url", urls[position]);
                intent.putExtra("className", classNames[position]);
                startActivity(intent);
            }
        });
    }
}
